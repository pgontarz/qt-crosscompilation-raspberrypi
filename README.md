**CrossCompilation**
* https://mechatronicsblog.com/cross-compile-and-deploy-qt-5-12-for-raspberry-pi/
* http://wapel.de/?p=641
* https://wiki.qt.io/Building_Qt_5_from_Git
* https://github.com/raulicomr/qt-rpi-macos (**MacOS**)
---
**WebEngine**
* https://ottycodes.wordpress.com/2020/01/27/cross-compiling-qt-5-12-3-with-qtwebengine-for-raspberry-pi-4-headache/
* https://wiki.qt.io/RaspberryPiWithWebEngine
* https://forum.qt.io/topic/93782/qtwebengine-for-raspberry-pi-3
* (**TO CHECK**) https://intestinate.com/pilfs/beyond.html
---
**Custom Compilator**
* http://blog.felipe.rs/2015/01/20/how-to-build-a-gcc-cross-compiler-for-the-raspberrypi/
---
**MQTT compilation**
* QT MQTT version 5.12.10 (https://github.com/qt/qtmqtt)
* https://stackoverflow.com/questions/48701475/qt-creator-adding-mqtt-library
* **FOR ANDROID** 
    * https://forum.qt.io/topic/115276/project-error-you-need-to-set-the-android_ndk_root-environment-variable-to-point-to-your-android-ndk/11 
    * export ANDROID_NDK_ROOT=/Users/macbook/Library/Android/sdk/ndk/21.1.6352462
* **FOR iOS**
    * https://www.xspdf.com/help/50566720.html
    * sudo xcode-select -s /Applications/Xcode.app/Contents/Developer

---
To **manually compile qt project** from terminal use:
* path_to_qmake path_to_PRO_file
    * example: /home/parallels/raspi/qt5/bin/qmake /home/parallels/Desktop/smart-home-gui/home_center/home_center.pro
---
To **run multiple scripts/programms** in RPi before desktop and with CLI interface:
##### sudo nano /etc/rc.local
* add sudo python3 /home/pi/Desktop/virtual_dashboard_project/dashboard_backend/main.py &
* sudo xinit /home/pi/Desktop/virtual_dashboard &
